import {ActionReducerMap, MetaReducer} from "@ngrx/store";
import {State as TodosState, reducer as todosReducer} from '../features/todos/todos.reducer';

export interface State {
    todos: TodosState
}

export const reducers: ActionReducerMap<State> = {
    todos: todosReducer
}

export const metaReducers: MetaReducer<State>[] = [];
