import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {EffectsModule} from '@ngrx/effects';
import {TodosEffects} from './features/todos/todos.effects';
import {StoreModule} from "@ngrx/store";
import {metaReducers, reducers} from "./reducers";
import { TodosListComponent } from './components/todos-list/todos-list.component';
import { TodosItemComponent } from './components/todos-item/todos-item.component';
import {MatListModule} from "@angular/material/list";
import {CommonModule} from "@angular/common";
import {environment} from "../environments/environment";
import {StoreDevtoolsModule} from "@ngrx/store-devtools";
import {HttpClientModule} from "@angular/common/http";
import { NewTodoFormComponent } from './components/new-todo-form/new-todo-form.component';

@NgModule({
    declarations: [
        AppComponent,
        TodosListComponent,
        TodosItemComponent,
        NewTodoFormComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        EffectsModule.forRoot([TodosEffects]),
        StoreModule.forRoot(reducers, {metaReducers}),
        MatListModule,
        HttpClientModule,
        CommonModule,
        !environment.production ? StoreDevtoolsModule.instrument({maxAge: 25, logOnly: environment.production}) : [],
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
