import {Component} from '@angular/core';
import {State} from "./reducers";
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";
import {TodoItem} from "./features/todos/todos.reducer";
import {selectAllTodos} from "./features/todos/todos.selectors";
import {addTodoItem, loadTodos} from "./features/todos/todos.actions";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    constructor(private readonly store$: Store<State>) {
        this.store$.dispatch(loadTodos());
    }

    private _todos$: Observable<TodoItem[] | null> = this.store$.select(selectAllTodos);

    get todos$(): Observable<TodoItem[] | null> {
        return this._todos$;
    }

    public addNewTodo(newTodo: TodoItem): void {
        this.store$.dispatch(addTodoItem({newTodo}));
    }
}
