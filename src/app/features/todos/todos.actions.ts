import {createAction, props} from '@ngrx/store';
import {TodoItem} from "./todos.reducer";

export const loadTodos = createAction(
    '[Todos] Load Todos',
);

export const todosLoaded = createAction(
    '[Todos] Todos Loaded',
    props<{ todosToAdd: TodoItem[] }>(),
);

export const addTodoItem = createAction(
    '[Todos] Add Todo Item',
    props<{ newTodo: TodoItem }>(),
);

export const todoItemAdded = createAction(
    '[Todos] Todo Item Added',
    props<{ newTodo: TodoItem }>(),
);

export const removeTodoItem = createAction(
    '[Todos] Remove Todo Item',
    props<{ todoToRemove: TodoItem }>(),
);

export const todoItemRemoved = createAction(
    '[Todos] Todo Item Removed',
    props<{ removedTodo: TodoItem }>(),
);

export const changeTodoItemStatus = createAction(
    '[Todos] Change Todo Item Status',
    props<{ todoToChange: TodoItem, readonly newStatus: string }>(),
);

export const todoItemUpdated = createAction(
    '[Todos] Todo Item Updated',
    props<{ todoToChange: TodoItem }>(),
);
