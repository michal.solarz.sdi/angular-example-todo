import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import * as TodosActions from './todos.actions';
import {of, switchMap} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {todoItemAdded, todosLoaded} from "./todos.actions";

@Injectable()
export class TodosEffects {

    loadTodos$ = createEffect(() => this.actions$.pipe(
        ofType(TodosActions.loadTodos),
        switchMap((action) => {
            // this.httpClient.get()
            const todos = [
                {
                    id: 1,
                    state: 'pending',
                    text: 'Todo 1'
                },
                {
                    id: 2,
                    state: 'pending',
                    text: 'Todo 2'
                },
                {
                    id: 3,
                    state: 'pending',
                    text: 'Todo 3'
                }
            ];
            return of(todosLoaded({todosToAdd: todos}));
        })
    ))

    addNewTodo$ = createEffect(() => this.actions$.pipe(
        ofType(TodosActions.addTodoItem),
        switchMap((action) => {
            // this.httpClient.post()
            return of(todoItemAdded({newTodo: action.newTodo}));
        })
    ))

    constructor(private actions$: Actions,
                private httpClient: HttpClient) {
    }
}
