import {createEntityAdapter, EntityAdapter, EntityState} from "@ngrx/entity";
import {createReducer, on} from "@ngrx/store";
import * as TodosActions from './todos.actions';

export const todosFeatureKey = 'todos';

export interface TodoItem {
    id: number;
    state: string;
    text: string;
}

export interface State {
    todos: EntityState<TodoItem>
}

export const adapter: EntityAdapter<TodoItem> = createEntityAdapter<TodoItem>({
    selectId: (item: TodoItem) => item.id,
});

export const initialState: State = {
    todos: adapter.getInitialState()
};

export const reducer = createReducer(
    initialState,
    on(TodosActions.todosLoaded, (state, {todosToAdd}) => ({...state, todos: adapter.setAll(todosToAdd, initialState.todos)})),
    on(TodosActions.todoItemAdded, (state, {newTodo}) => ({...state, todos: adapter.setOne(newTodo, state.todos)})),
    on(TodosActions.todoItemRemoved, (state, {removedTodo}) => ({...state, todos: adapter.removeOne(removedTodo.id, state.todos)})),
    on(TodosActions.todoItemUpdated, (state, {todoToChange}) => ({...state, todos: adapter.upsertOne(todoToChange, state.todos)})),
    )

export const {
    selectAll,
} = adapter.getSelectors();
