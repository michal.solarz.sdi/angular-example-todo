import {createFeatureSelector, createSelector} from '@ngrx/store';
import * as fromTodos from './todos.reducer';
import {selectAll, State as TodosState} from './todos.reducer';

export const selectTodosState = createFeatureSelector<fromTodos.State>(
    fromTodos.todosFeatureKey
);

export const selectAllTodos = createSelector(
    selectTodosState,
    (state: TodosState) => selectAll(state.todos));
