import {Component, ElementRef, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {Store} from "@ngrx/store";
import {State} from "../../reducers";
import {TodoItem} from "../../features/todos/todos.reducer";

@Component({
    selector: 'app-new-todo-form',
    templateUrl: './new-todo-form.component.html',
    styleUrls: ['./new-todo-form.component.scss']
})
export class NewTodoFormComponent implements OnInit {
    @ViewChild('newTodoInput') newTodoInput: ElementRef<HTMLInputElement> | undefined;

    @Output() todoAdded = new EventEmitter();

    constructor(private store$: Store<State>) {
    }

    ngOnInit(): void {
    }

    public addNewTodo(): void {
        const newTodo: TodoItem = {
            id: 1,
            state: 'pending',
            text: this.newTodoInput ? this.newTodoInput.nativeElement.value : ''
        }
        this.todoAdded.emit(newTodo);
        this.newTodoInput ? this.newTodoInput.nativeElement.value = '' : null;
    }
}
