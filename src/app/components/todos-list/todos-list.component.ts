import {Component, Input, OnInit} from '@angular/core';
import {TodoItem} from "../../features/todos/todos.reducer";

@Component({
    selector: 'app-todos-list',
    templateUrl: './todos-list.component.html',
    styleUrls: ['./todos-list.component.scss']
})
export class TodosListComponent implements OnInit {

    constructor() {
    }

    private _todos: TodoItem[] | null = [];

    get todos(): TodoItem[] | null {
        return this._todos;
    }

    @Input()
    set todos(value: TodoItem[] | null) {
        this._todos = value;
    }

    ngOnInit(): void {
    }
}
