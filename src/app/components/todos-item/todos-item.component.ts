import {Component, Input, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {State} from "../../reducers";
import {TodoItem} from "../../features/todos/todos.reducer";

@Component({
    selector: 'app-todos-item',
    templateUrl: './todos-item.component.html',
    styleUrls: ['./todos-item.component.scss']
})
export class TodosItemComponent implements OnInit {

    constructor(private readonly store$: Store<State>) {
    }

    private _todo: TodoItem | null = null;

    get todo(): TodoItem | null {
        return this._todo;
    }

    @Input()
    set todo(value: TodoItem | null) {
        this._todo = value;
    }

    ngOnInit(): void {
    }
}
